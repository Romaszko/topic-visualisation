
# coding: utf-8

# Kod na podstawie: https://shuaiw.github.io/2016/12/22/topic-modeling-and-tsne-visualzation.html

import numpy as np
import lda
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.manifold import TSNE
import bokeh.plotting as bp
import bokeh
from bokeh.plotting import save
from bokeh.models import HoverTool
import matplotlib.pyplot as plt 

subset_lda_polish_wiki = np.load("polish_wiki_dataset/full_lda_50t_polish_wiki_from_dict.npy").item()
X_topics_a = subset_lda_polish_wiki.get_topics()
X_topics_b = list(zip(*X_topics_a))
X_topics = np.array(X_topics_b)

# threshold is not unnecessary in this case

threshold = 0.003
_idx = np.amax(X_topics, axis=1) > threshold  # idx of doc that above the threshold
X_topics = X_topics[_idx]

# a t-SNE model
# angle value close to 1 means sacrificing accuracy for speed
# pca initializtion usually leads to better results 
tsne_model = TSNE(n_components=2, verbose=1, random_state=0, angle=.99, init='pca')

# *-D -> 2-D
tsne_lda = tsne_model.fit_transform(X_topics)



n_top_words = 5 # number of keywords we show

# 50 colors
colormap = np.array([
    "#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c",
    "#98df8a", "#d62728", "#ff9896", "#9467bd", "#c5b0d5",
    "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f",
    "#c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5",
    "#1f77b4", "#aec7e8", "#ff7f0e", "#ffbb78", "#2ca02c",
    "#98df8a", "#d62728", "#ff9896", "#9467bd", "#c5b0d5",
    "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f",
    "#c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5",
    "#8c564b", "#c49c94", "#e377c2", "#f7b6d2", "#7f7f7f",
    "#c7c7c7", "#bcbd22", "#dbdb8d", "#17becf", "#9edae5",
])

_lda_keys = []
for i in range(X_topics.shape[0]):
  _lda_keys.append(X_topics[i].argmax())

topic_summaries = subset_lda_polish_wiki.show_topics(num_topics=50, num_words=3, formatted=False)
topic_summaries = list(map(lambda x: x[1], topic_summaries))
topic_summaries = list(map(lambda x: ' '.join(list(map(lambda y: y[0], x))), topic_summaries))

#topic_summaries = list(map(lambda x: ' '.join(x.split('"')[1::2]), topic_summaries))
  
title = 'Polish Wiki Full - Topic Visualization'
num_example = len(X_topics)

plot_lda = bp.figure(plot_width=1400, plot_height=1100,
                     title=title,
                     tools="pan,wheel_zoom,box_zoom,reset,hover,previewsave",
                     x_axis_type=None, y_axis_type=None, min_border=1)

plot_lda.scatter(x=tsne_lda[:,0], y=tsne_lda[:, 1], 
                 color=colormap[_lda_keys][:num_example])
bokeh.io.show(plot_lda, notebook_handle=True)



# randomly choose a news (within a topic) coordinate as the crucial words coordinate
topic_coord = np.empty((X_topics.shape[1], 2)) * np.nan
for topic_num in _lda_keys:
  if not np.isnan(topic_coord).any():
    break
  topic_coord[topic_num] = tsne_lda[_lda_keys.index(topic_num)]

# plot crucial words
for i in range(X_topics.shape[1]):
  plot_lda.text(topic_coord[i, 0], topic_coord[i, 1], [topic_summaries[i]])

# hover tools
hover = plot_lda.select(dict(type=HoverTool))
hover.tooltips = {"content": "@content - topic: @topic_key"}

# save the plot
save(plot_lda, '{}.html'.format(title))