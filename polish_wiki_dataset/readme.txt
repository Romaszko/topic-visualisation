# W pliku subset_lda_50t_polish_wiki.npy znajduje się model LDA zawierający 50 tematów
# na podstawie 1% tekstów z polskiej Wikipedii

# Model można załadować następująco:
subset_lda_polish_wiki = np.load("subset_lda_50t_polish_wiki.npy").item()

#A następnie korzystać z metod opisanych w dokumentacji:
# https://radimrehurek.com/gensim/models/ldamodel.html
# np.
subset_lda_polish_wiki.show_topics()